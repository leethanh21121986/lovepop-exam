terraform {
  backend "local" {
    path = "./tfstate/terraform.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.63.0"
    }
  }
}


provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Environment = "sandbox"
      Application = "lovepop-examination"
      ManagedBy   = "Terraform"
    }
  }
}