output "ecs_cluster_arn" {
    description = "Output ECS cluster arn"
    value       = aws_ecs_cluster.ecs_cluster.arn
}

output "task_definition_arn" {
  description = "Full ARN of the Task Definition (including both family and revision)."
  value = { 
    for key, config in var.service_config : key => aws_ecs_task_definition.ecs_task_definition[key].arn
  }
}

output "task_definition_family" {
  description = "The family of the Task Definition."
  value = { 
    for key, config in var.service_config : key => aws_ecs_task_definition.ecs_task_definition[key].family
  }
}

output "task_definition_revision" {
  description = "The revision of the task in a particular family."
  value = { 
    for key, config in var.service_config : key => aws_ecs_task_definition.ecs_task_definition[key].revision
  }
}

output "service_name" {
  description = "The name of the service"
  value = { 
    for key, config in var.service_config : key => aws_ecs_service.ecs_service[key].name
  }
}