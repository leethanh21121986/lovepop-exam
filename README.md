# lovepop-exam

## Terraform Structure ##
```
Modules:
- apigateway
- cloudwatch
- ecs
- elb
- iam
- securitygroup
- sns
```

## API GATEWAY ##
```
"LOVEPOP_API_GW.json" includes API Gateway's configuration
```

## Result ##
```
The results will be stored in terraform-plan file
```

