resource "aws_api_gateway_rest_api" "api" {
  count                        = var.create ? 1 : 0
  name                         = var.name
  description                  = var.description != "" ? var.description : null
  disable_execute_api_endpoint = var.disable_execute_api_endpoint == true ? true : false
  binary_media_types           = var.binary_media_types != "" ? var.binary_media_types : null
  minimum_compression_size     = var.minimum_compression_size
  api_key_source               = var.api_key_source
  body                         = var.body.content-file

  endpoint_configuration {
    types            = var.types
    vpc_endpoint_ids = length(var.vpc_endpoint_ids) > 0 && var.vpc_endpoint_ids[0] != "" ? var.vpc_endpoint_ids : null
  }

  policy = var.api_policy != "" ? var.api_policy : null
  tags   = var.tags
}