variable cluster_name {}
variable service_config { default = {} }
variable ecs_task_execution_role_arn { default = {} }
variable region { default = {} }
variable tags {
  default = {}
}
