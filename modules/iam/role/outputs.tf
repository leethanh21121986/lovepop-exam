output "role_id" {
    value = aws_iam_role.iam_role.id
}

output "role_name" {
    value = aws_iam_role.iam_role.name
}

output "role_arn" {
    value = aws_iam_role.iam_role.arn
}

output "role_create_date" {
    value = aws_iam_role.iam_role.create_date
}

output "role_unique_id" {
    value = aws_iam_role.iam_role.unique_id
}

# output "profile_arn" {
#     value = aws_iam_instance_profile.iam-instance-profile[0].arn
# }