output "id" {
  value       = join("", aws_api_gateway_rest_api.api.*.id)
  description = "The ID of the REST API."
}

output "root_resource_id" {
  value       = join("", aws_api_gateway_rest_api.api.*.root_resource_id)
  description = "The ROOT Resource ID of the REST API."
}

output "execution_arn" {
  value       = join("", aws_api_gateway_rest_api.api.*.execution_arn)
  description = "The Execution ARN of the REST API."
}