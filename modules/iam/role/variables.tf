variable "role_name" {
  default = {}
}

variable "profile_name" {
  default = null
}

variable "policy_arns" {
  default = {}
}

variable "assume_policy" {
  default = {}
}

variable "inline_policy" {
  default = {}
}

variable "tags" {
  default = {}
}
