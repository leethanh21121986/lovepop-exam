variable "create" {
  description = "Controls if API Gateway resources should be created"
  type        = bool
  default     = true
}

variable "name" {
  type        = string
  default     = ""
  description = "Name  (e.g. `app` or `cluster`)."
}

variable "description" {
  type        = string
  default     = ""
  description = "The description of the REST API "
}

variable "api_key_source" {
  type        = string
  default     = "HEADER"
  description = "The source of the API key for requests. Valid values are HEADER (default) and AUTHORIZER."
}

variable "binary_media_types" {
  type        = list(any)
  default     = ["UTF-8-encoded"]
  description = "The list of binary media types supported by the RestApi. By default, the RestApi supports only UTF-8-encoded text payloads."
}

variable "vpc_endpoint_ids" {
  type        = list(string)
  default     = ["", ]
  description = "Set of VPC Endpoint identifiers. It is only supported for PRIVATE endpoint type."
}

variable "api_policy" {
  default     = null
  description = "The policy document."
}

variable "disable_execute_api_endpoint" {
  type        = bool
  default     = false
  description = "Enable or Disable default execute-api endpoint."
}

variable "types" {
  type        = list(any)
  default     = ["REGIONAL"] # Accepted values ["EDGE", "REGIONAL", "PRIVATE"]
  description = "Whether to create rest api."
}

variable "minimum_compression_size" {
  type        = number
  default     = -1
  description = "Minimum response size to compress for the REST API. Integer between -1 and 10485760 (10MB). Setting a value greater than -1 will enable compression, -1 disables compression (default)."
}

variable "tags" {
  type        = map(any)
  default     = {}
  description = "A mapping of tags to assign to API gateway resources."
}

variable "region" {
  type        = string
  default     = "ap-southeast-1"
  description = "AWS region for resource deployment."
}

variable "body" {
  description = "OpenAPI specification that defines the set of routes and integrations to create as part of the REST API"
  default     = {}
}
