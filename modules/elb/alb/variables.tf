#Variables referenced in ALB root module

variable alb_name {}
variable is_internal {}
variable lb_type {}
variable sg_id { default = [] }
variable tags { default = {} }
variable subnet_ids { default = [] }
variable delete_protection {}
variable app_prefix {}
variable idle_timeout { default = 60 }

