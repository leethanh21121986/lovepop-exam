module "iam_role_01" {
  source        = "./modules/iam/role"
  role_name     = local.iam.role_01.name
  assume_policy = local.iam.role_01.assume_policy
  policy_arns   = local.iam.role_01.policy_arns
}

module "sg_01" {
  source      = "./modules/securitygroup"
  name        = local.securitygroup.sg_01.name
  description = local.securitygroup.sg_01.description
  vpc_id      = local.securitygroup.sg_01.vpc_id
  ingress_with_cidr_blocks = concat(
    local.securitygroup.sg_01.cidr_ingress
  )
  egress_with_cidr_blocks = local.securitygroup.egress_default
}

module "tg_01" {
  source       = "./modules/elb/target-group"
  name         = local.target_group.tg_01.name
  port         = local.target_group.tg_01.port
  protocol     = local.target_group.tg_01.protocol
  target_type  = local.target_group.tg_01.target_type
  target_id    = local.target_group.tg_01.target_id
  health_check = local.target_group.tg_01.health_check
  vpc_id       = local.target_group.tg_01.vpc_id
  tags         = local.tags
}

module "ecs_cluster_01" {
  source                      = "./modules/ecs/cluster"
  cluster_name                = local.ecs.cluster.cluster_01.cluster_name
  service_config              = local.ecs.cluster.cluster_01.microservice_config
  ecs_task_execution_role_arn = local.ecs.cluster.cluster_01.role_arn
  region                      = local.ecs.cluster.cluster_01.region
}

module "alb_01" {
  source            = "./modules/elb/alb"
  alb_name          = local.alb.lb_01.name
  is_internal       = local.alb.lb_01.is_internal
  lb_type           = local.alb.lb_01.lb_type
  sg_id             = local.alb.lb_01.sg_id
  subnet_ids        = local.alb.lb_01.subnet_ids
  app_prefix        = local.alb.lb_01.app_prefix
  delete_protection = local.alb.lb_01.delete_protection
  depends_on = [
    module.sg_01,
    module.tg_01
  ]
}

module "lsnr_01" {
  source            = "./modules/elb/listener"
  load_balancer_arn = module.alb_01.arn
  port              = local.alb.lb_01.listener.lsnr_01.port
  protocol          = local.alb.lb_01.listener.lsnr_01.protocol
  default_action    = local.alb.lb_01.listener.lsnr_01.default_action
  depends_on        = [module.tg_01]
}

module "sns_subscription_state_change" {
  source         = "./modules/sns"
  sns_topic_name = "lovepop-api-change-alarm"
  subscribers    = ["devops@lovepop.com"]
}

module "sns_subscription_task_restart" {
  source         = "./modules/sns"
  sns_topic_name = "lovepop-api-restart-alarm"
  subscribers    = ["devops@lovepop.com"]
}


module "ecs_cloudwatch_alerts" {
  source    = "./modules/cloudwatch/metric-alarm"
  namespace = "lovepop"
  stage     = "sandbox"

  cluster_arn                   = module.ecs_cluster_01.ecs_cluster_arn
  ecs_task_restart_service_name = values(module.ecs_cluster_01.service_name)
  ecs_task_restart_task_name    = values(module.ecs_cluster_01.task_definition_family)

  ecs_task_definition_arn = values(module.ecs_cluster_01.task_definition_arn)

  sns_topic_ecs_state_change_arn = module.sns_subscription_state_change.aws_sns_topic_arn
  sns_topic_ecs_task_stopped_arn = module.sns_subscription_state_change.aws_sns_topic_arn
  task_restart_alarm_actions     = [module.sns_subscription_task_restart.aws_sns_topic_arn]
}

module "apigw" {
  source                       = "./modules/apigateway"
  name                         = local.apigateway.apigw_01.name
  description                  = local.apigateway.apigw_01.description
  api_policy                   = local.apigateway.apigw_01.api_policy
  api_key_source               = local.apigateway.apigw_01.api_key_source
  binary_media_types           = local.apigateway.apigw_01.binary_media_types
  disable_execute_api_endpoint = local.apigateway.apigw_01.disable_execute_api_endpoint
  minimum_compression_size     = local.apigateway.apigw_01.minimum_compression_size
  types                        = local.apigateway.apigw_01.endpoint_configuration.types
  body                         = local.apigateway.apigw_01.body
}