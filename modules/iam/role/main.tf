resource "aws_iam_role" "iam_role" {
	name                  = var.role_name
	assume_role_policy    = var.assume_policy.policy_file
	inline_policy {
		name = try(var.inline_policy.name, null)
		policy = try(tostring(var.inline_policy.policy_file), null)
	}
	tags = merge(var.tags,{
      Name = var.role_name
  })
}

resource "aws_iam_role_policy_attachment" "policy_attachment" {
	for_each              = var.policy_arns
	role                  = aws_iam_role.iam_role.name
	policy_arn            = each.value.policy_arn
}

# Create instance profile
resource "aws_iam_instance_profile" "iam-instance-profile" {
  count = var.profile_name != null ? 1 : 0
  name = var.profile_name
  role = aws_iam_role.iam_role.name
}