variable name {}
variable port {}
variable tags {}
variable vpc_id {}
variable target_id {}
variable protocol {}
variable target_type {}
variable availability_zone {
  default =""
}
variable sticky_type {
  default = "source_ip"
}
variable enable_sticky {
  default = false
}
variable health_check {
  default = {
    interval            = 30
    path                = "/"
    protocol            = "HTTPS"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
    matcher             = "200-302"
  }
}