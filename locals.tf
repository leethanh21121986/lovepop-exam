locals {
  ### TAGS ###
  tags = {
    ApplicationName = "lovepop"
    Environment     = "sandbox"
    IACManaged      = "terraform"
  }

  ### NETWORKING ###
  vpc = {
    id          = "vpc-093c0683b30932b11"
    cidr_block  = "10.0.0.0/16"
    lovepop-az1 = "subnet-02b4db779345b5af9"
    lovepop-az2 = "subnet-0c363b8e4efec0d3c"
    lovepop-az3 = "subnet-0d6837d9e0f0a678e"
  }

  ### IAM ROLE ###
  iam = {
    role_01 = {
      name = "LOVEPOP_ECSTASK_ROLE"
      assume_policy = {
        policy_file = file("policies/ecs/cluster_01/LOVEPOP_ECSTASK_ASSUME_POLICY.json")
      }
      policy_arns = {
        "AmazonECSTaskExecutionRolePolicy" = {
          policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
        }
      }
    }
  }

  ### SECURITY GROUP ###
  securitygroup = {
    sg_01 = {
      name        = "LOVEPOP-API-ECS-SG"
      description = "LOVEPOP ECS cluster and load balancer security group for API"
      vpc_id      = local.vpc.id
      cidr_ingress = [{
        from_port   = "1200"
        to_port     = "1200"
        protocol    = "tcp"
        description = "inbound TCP 1200 access"
        cidr_blocks = local.vpc.cidr_block
      }]
    },
    egress_default = [{
      from_port   = 0
      to_port     = 0
      protocol    = -1
      description = "All traffic outbound rule"
      cidr_blocks = "0.0.0.0/0"
    }]
  }

  ### TARGET GROUP ###
  target_group = {
    tg_01 = {
      name        = "LOVEPOP-API-TG"
      port        = "1200"
      protocol    = "HTTP"
      vpc_id      = local.vpc.id
      target_type = "ip"
      target_id   = ""
      health_check = {
        interval            = 300
        path                = "/"
        protocol            = "HTTP"
        timeout             = 120
        healthy_threshold   = 5
        unhealthy_threshold = 4
        matcher             = 200
      }
    }
  }

  ### ECS CLUSTER & TASK DEFINATION ###
  ecs = {
    cluster = {
      cluster_01 = {
        cluster_name = "LOVEPOP-API-ECS"
        role_arn     = module.iam_role_01.role_arn
        region       = "us-east-1"
        microservice_config = {
          LOVEPOP-DS-Task = {
            name             = "LOVEPOP-Container"
            service_name     = "LOVEPOP-Service"
            memory           = 30720
            cpu              = 4096
            image            = "407331559142.dkr.ecr.us-east-1.amazonaws.com/lovepop-api-ecr:port1200"
            host_port        = 1200
            container_port   = 1200
            desired_count    = 1
            subnets          = [local.vpc.lovepop-az1]
            security_groups  = [module.sg_01.this_security_group_id]
            target_group_arn = module.tg_01.tg_arn
            environment = {
              name  = "ASPNETCORE_ENVIRONMENT"
              value = "Staging"
            }
            auto_scaling = {
              max_capacity = 3
              min_capacity = 1
              cpu = {
                target_value = 75
              }
              memory = {
                target_value = 75
              }
            }
          }
        }
      }
    }
  }

  ### LOAD BALANCER ###
  alb = {
    lb_01 = {
      name              = "API-ALB"
      is_internal       = false
      lb_type           = "application"
      sg_id             = [module.sg_01.this_security_group_id]
      subnet_ids        = [local.vpc.lovepop-az1, local.vpc.lovepop-az2, local.vpc.lovepop-az3]
      app_prefix        = "LOVEPOP"
      delete_protection = true
      listener = {
        lsnr_01 = {
          port     = 80
          protocol = "HTTP"
          default_action = {
            type = "forward"
            forward = {
              target_groups = {
                target_group = {
                  arn = module.tg_01.tg_arn
                }
              }
            }
          }
        }
      }
    }
  }

  ### API ###
  apigateway = {
    apigw_01 = {
      name                         = "LOVEPOP-REST-API-GW"
      description                  = "LOVEPOP HTTP API Gateway"
      api_policy                   = null
      api_key_source               = "HEADER"
      binary_media_types           = ["UTF-8-encoded", "application/zip", "binary/*"]
      disable_execute_api_endpoint = true
      minimum_compression_size     = 0
      vpc_link                     = {}
      endpoint_configuration = {
        types = ["REGIONAL"] # possible values EDGE, REGIONAL or PRIVATE
        # vpc_endpoint_ids = [] # only applicable for PRIVATE type
      }
      body = {
        content-file = file("LOVEPOP_API_GW.json")
      }
    }
  }
}


      